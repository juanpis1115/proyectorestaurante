package com.juanquirogaconexia.web.Controlador;

import java.io.IOException;
import com.juanquirogaconexia.web.Variables.VariablesCliente;
import com.juanquirogaconexia.web.Controlador.ClienteOperaciones;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**

 * Servlet implementation class GetAlienController

 */

public class ServletCliente extends HttpServlet {

	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		

		int Dinero = Integer.parseInt(request.getParameter("Dinero"));

		ClienteOperaciones op  = new ClienteOperaciones();

		VariablesCliente VarCli = op.getCliente(Dinero);

		

		HttpSession session = request.getSession();

		session.setAttribute("Cliente", VarCli);

		

		response.sendRedirect("MostrarCliente.jsp");

	}



	



}
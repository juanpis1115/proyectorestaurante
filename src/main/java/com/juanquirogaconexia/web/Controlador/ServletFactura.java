package com.juanquirogaconexia.web.Controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.juanquirogaconexia.web.Variables.VariablesCliente;
import com.juanquirogaconexia.web.Variables.VariablesFactura;

/**
 * Servlet implementation class ServletFactura
 */
public class ServletFactura extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int IdFactura = Integer.parseInt(request.getParameter("IdFactura"));
		int Camarero = Integer.parseInt(request.getParameter("IdCamarero"));
		int Mesa = Integer.parseInt(request.getParameter("IdMesa"));
		String Plato1= request.getParameter("Plato1");
		String Plato2= request.getParameter("Plato2");
		String Plato3= request.getParameter("Plato3");
		String Plato4= request.getParameter("Plato4");
		String Plato5= request.getParameter("Plato5");

		

		FacturaOperaciones op  = new FacturaOperaciones();

		VariablesFactura VarFact = op.setFactura(IdFactura, Camarero, Mesa, Plato1, Plato2, Plato3, Plato4, Plato5);

		

		HttpSession session = request.getSession();
 
		session.setAttribute("Factura", VarFact);

	}



	



}

	
package com.juanquirogaconexia.web.Controlador;

import com.juanquirogaconexia.web.Variables.VariablesCliente;
import com.juanquirogaconexia.web.Variables.VariablesConexion;

import java.sql.*;

public class ClienteOperaciones {
	
	public VariablesCliente getCliente(int Dinero) {
		
		VariablesCliente Cl = new VariablesCliente();
		VariablesConexion conex= new VariablesConexion();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://"+conex.getUrl()+":"+conex.getPuerto()+"/"+conex.getBaseDeDatos(),conex.getUsuario(),conex.getContraseña());
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from cliente where DineroInvertido>="+Dinero);
			
			if(rs.next()) {
				Cl.setIdCliente(rs.getInt("IdCliente"));
				Cl.setNombreCliente(rs.getString("Nombre"));
				Cl.setApellido1Cliente(rs.getString("Apellido1"));
				Cl.setApellido2Cliente(rs.getString("Apellido2"));
				Cl.setDineroInvertido(rs.getInt("DineroInvertido"));
				
			}
			
			}
		catch (Exception e) {
			System.out.println(e);
		}
		
		return Cl;
	}

}

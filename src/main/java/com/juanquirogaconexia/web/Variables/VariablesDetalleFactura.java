package com.juanquirogaconexia.web.Variables;

public class VariablesDetalleFactura {

	private int IdFactura, IdDetalleFactura, IdCocinero;
	private String Plato, Importe;
	public int getIdFactura() {
		return IdFactura;
	}
	public void setIdFactura(int idFactura) {
		IdFactura = idFactura;
	}
	public int getIdDetalleFactura() {
		return IdDetalleFactura;
	}
	public void setIdDetalleFactura(int idDetalleFactura) {
		IdDetalleFactura = idDetalleFactura;
	}
	public int getIdCocinero() {
		return IdCocinero;
	}
	public void setIdCocinero(int idCocinero) {
		IdCocinero = idCocinero;
	}
	public String getPlato() {
		return Plato;
	}
	public void setPlato(String plato) {
		Plato = plato;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	@Override
	public String toString() {
		return "VariablesDetalleFactura [IdFactura=" + IdFactura + ", IdDetalleFactura=" + IdDetalleFactura
				+ ", IdCocinero=" + IdCocinero + ", Plato=" + Plato + ", Importe=" + Importe + "]";
	}
	
}

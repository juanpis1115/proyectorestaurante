package com.juanquirogaconexia.web.Variables;

public class VariablesCliente {
	
	private int IdCliente, DineroInvertido;
	private String NombreCliente, Apellido1Cliente, Apellido2Cliente;
	public int getIdCliente() {
		return IdCliente;
	}
	public int getDineroInvertido() {
		return DineroInvertido;
	}
	public void setDineroInvertido(int dineroInvertido) {
		DineroInvertido = dineroInvertido;
	}
	public void setIdCliente(int idCliente) {
		IdCliente = idCliente;
	}
	public String getNombreCliente() {
		return NombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		NombreCliente = nombreCliente;
	}
	public String getApellido1Cliente() {
		return Apellido1Cliente;
	}
	public void setApellido1Cliente(String apellido1Cliente) {
		Apellido1Cliente = apellido1Cliente;
	}
	public String getApellido2Cliente() {
		return Apellido2Cliente;
	}
	public void setApellido2Cliente(String apellido2Cliente) {
		Apellido2Cliente = apellido2Cliente;
	}
	@Override
	public String toString() {
		return "VariablesCliente [IdCliente=" + IdCliente + ", NombreCliente=" + NombreCliente + ", Apellido1Cliente="
				+ Apellido1Cliente + ", Apellido2Cliente=" + Apellido2Cliente + "]";
	}
	

}

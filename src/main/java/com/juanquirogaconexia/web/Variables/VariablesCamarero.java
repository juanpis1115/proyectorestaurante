package com.juanquirogaconexia.web.Variables;

public class VariablesCamarero {

	private int IdCamarero;
	private String Nombre, Apellido1Camarero, Apellido2Camarero;
	public int getIdCamarero() {
		return IdCamarero;
	}
	public void setIdCamarero(int idCamarero) {
		IdCamarero = idCamarero;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido1Camarero() {
		return Apellido1Camarero;
	}
	public void setApellido1Camarero(String apellido1Camarero) {
		Apellido1Camarero = apellido1Camarero;
	}
	public String getApellido2Camarero() {
		return Apellido2Camarero;
	}
	public void setApellido2Camarero(String apellido2Camarero) {
		Apellido2Camarero = apellido2Camarero;
	}
	@Override
	public String toString() {
		return "VariablesCamarero [IdCamarero=" + IdCamarero + ", Nombre=" + Nombre + ", Apellido1Camarero="
				+ Apellido1Camarero + ", Apellido2Camarero=" + Apellido2Camarero + "]";
	}
	
	
}

package com.juanquirogaconexia.web.Variables;

public class VariablesConexion {

	private int puerto= 3306;
	private String Url= "localhost";
	private String Usuario="root";
	private String Contraseña="";
	private String BaseDeDatos="lamejorcocina";
	public String getBaseDeDatos() {
		return BaseDeDatos;
	}
	public void setBaseDeDatos(String baseDeDatos) {
		BaseDeDatos = baseDeDatos;
	}
	public int getPuerto() {
		return puerto;
	}
	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}
	public String getUrl() {
		return Url;
	}
	public void setUrl(String url) {
		Url = url;
	}
	public String getUsuario() {
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public String getContraseña() {
		return Contraseña;
	}
	public void setContraseña(String contraseña) {
		Contraseña = contraseña;
	}
	@Override
	public String toString() {
		return "VariablesConexion [puerto=" + puerto + ", Url=" + Url + ", Usuario=" + Usuario + ", Contraseña="
				+ Contraseña + ", BaseDeDatos=" + BaseDeDatos + "]";
	}
	
}

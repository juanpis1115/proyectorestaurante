package com.juanquirogaconexia.web.Variables;

public class VariablesFactura {
	
	private int IdFactura, IdCliente, IdCamarero, IdMesa;
	private String FechaFactura;
	public int getIdFactura() {
		return IdFactura;
	}
	public void setIdFactura(int idFactura) {
		IdFactura = idFactura;
	}
	public int getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(int idCliente) {
		IdCliente = idCliente;
	}
	public int getIdCamarero() {
		return IdCamarero;
	}
	public void setIdCamarero(int idCamarero) {
		IdCamarero = idCamarero;
	}
	public int getIdMesa() {
		return IdMesa;
	}
	public void setIdMesa(int idMesa) {
		IdMesa = idMesa;
	}
	public String getFechaFactura() {
		return FechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		FechaFactura = fechaFactura;
	}
	@Override
	public String toString() {
		return "VariablesFactura [IdFactura=" + IdFactura + ", IdCliente=" + IdCliente + ", IdCamarero=" + IdCamarero
				+ ", IdMesa=" + IdMesa + ", FechaFactura=" + FechaFactura + "]";
	}
	

}

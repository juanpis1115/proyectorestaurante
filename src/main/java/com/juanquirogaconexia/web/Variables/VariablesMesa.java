package com.juanquirogaconexia.web.Variables;

public class VariablesMesa {

	private int IdMesa, NumComensales;
	private String Ubicacion;
	public int getIdMesa() {
		return IdMesa;
	}
	public void setIdMesa(int idMesa) {
		IdMesa = idMesa;
	}
	public int getNumComensales() {
		return NumComensales;
	}
	public void setNumComensales(int numComensales) {
		NumComensales = numComensales;
	}
	public String getUbicacion() {
		return Ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		Ubicacion = ubicacion;
	}
	@Override
	public String toString() {
		return "VariablesMesa [IdMesa=" + IdMesa + ", NumComensales=" + NumComensales + ", Ubicacion=" + Ubicacion
				+ "]";
	}
}

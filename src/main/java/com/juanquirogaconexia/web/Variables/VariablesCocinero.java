package com.juanquirogaconexia.web.Variables;

public class VariablesCocinero {
	
	private int IdCocinero;
	private String NombreCocinero, Apellido1Cocinero, Apellido2Cocinero;
	public int getIdCocinero() {
		return IdCocinero;
	}
	public void setIdCocinero(int idCocinero) {
		IdCocinero = idCocinero;
	}
	public String getNombreCocinero() {
		return NombreCocinero;
	}
	public void setNombreCocinero(String nombreCocinero) {
		NombreCocinero = nombreCocinero;
	}
	public String getApellido1Cocinero() {
		return Apellido1Cocinero;
	}
	public void setApellido1Cocinero(String apellido1Cocinero) {
		Apellido1Cocinero = apellido1Cocinero;
	}
	public String getApellido2Cocinero() {
		return Apellido2Cocinero;
	}
	public void setApellido2Cocinero(String apellido2Cocinero) {
		Apellido2Cocinero = apellido2Cocinero;
	}
	@Override
	public String toString() {
		return "VariablesCocinero [IdCocinero=" + IdCocinero + ", NombreCocinero=" + NombreCocinero
				+ ", Apellido1Cocinero=" + Apellido1Cocinero + ", Apellido2Cocinero=" + Apellido2Cocinero + "]";
	}
	

}
